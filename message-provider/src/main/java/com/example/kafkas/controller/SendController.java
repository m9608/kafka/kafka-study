package com.example.kafkas.controller;

import com.example.kafkas.constant.KafkaConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Administrator
 * @Date 2024/4/3 10:11
 * @Description
 **/
@Slf4j
@RestController
@RequestMapping("/send")
public class SendController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;


    @PostMapping
    public String sendMsg(String msg) {
        kafkaTemplate.send(KafkaConstant.TOPIC_TEST, msg);
        return msg;
    }

    @PostMapping("/{topic}")
    public String sendTopicMsg(@PathVariable String topic, String msg) {
        kafkaTemplate.send(topic, msg);
        return msg;
    }
}
