package com.example.kafkas.consumer;

import com.example.kafkas.constant.KafkaConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @Author Administrator
 * @Date 2024/4/3 10:15
 * @Description
 **/
@Slf4j
@Component
public class TestConsumer {

    @KafkaListener(topics = KafkaConstant.TOPIC_TEST)
    public void listen(ConsumerRecord<String, String> record) {
        String value = record.value();
        String key = record.key();
        log.info("key={}, value = {}",key, value);
    }
}