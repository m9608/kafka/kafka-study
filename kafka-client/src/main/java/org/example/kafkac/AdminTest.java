package org.example.kafkac;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DeleteTopicsOptions;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.common.KafkaFuture;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class AdminTest {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "172.18.248.111:9092");
//        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        AdminClient kafkaAdminClient;
        try{
             kafkaAdminClient = KafkaAdminClient.create(properties);
            List<String> topicList = new ArrayList<>();
//            for (int i = 1; i< 10; i++){
//                topicList.add("__delay-hours-1");
//            }
            topicList.add("__delay-hours-1");

            DeleteTopicsResult deleteTopicsResult = kafkaAdminClient.deleteTopics(topicList);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
