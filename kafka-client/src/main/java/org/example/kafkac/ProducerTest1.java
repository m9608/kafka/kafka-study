package org.example.kafkac;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.HashMap;
import java.util.Map;

public class ProducerTest1 {
    public static void main(String[] args) {
        // 创建配置对象
        Map<String, Object> configMap= new HashMap<>();
        configMap.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"172.18.248.111:9092");
        configMap.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        configMap.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());
        // 创建生产者
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(configMap);
        for (int i = 11; i < 20; i++) {
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>("test-will", "key"+i,"asdasasdfd" + i);

            kafkaProducer.send(producerRecord);
        }

        // 关闭
        kafkaProducer.close();
    }
}
